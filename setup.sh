#!/bin/sh
sudo apt-get install wget unzip texlive-xetex texlive-fonts-extra texlive-bibtex-extra pandoc fonts-symbola biber make openjdk-8-jdk

wget https://github.com/Peeragogy/peeragogy-handbook/archive/master.zip
unzip ./master.zip
mv ./peeragogy-handbook-master/ ./peeragogy-handbook/
rm master.zip

wget https://github.com/Peeragogy/Peeragogy.github.io/archive/master.zip
unzip ./master.zip
mv ./Peeragogy.github.io-master/ ./peeragogy-handbook/Peeragogy.github.io/
rm master.zip

wget https://gitlab.com/publishing-systems/automated_digital_publishing/-/archive/master/automated_digital_publishing-master.zip
unzip automated_digital_publishing-master.zip
mv ./automated_digital_publishing-master/ ./automated_digital_publishing/
cd ./automated_digital_publishing/
make
cd workflows
java setup1
cd ..
cd ..

wget https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/-/archive/master/digital_publishing_workflow_tools-master.zip
unzip digital_publishing_workflow_tools-master.zip
mv ./digital_publishing_workflow_tools-master/ ./digital_publishing_workflow_tools/
cd ./digital_publishing_workflow_tools/
make
cd workflows
cd setup
cd setup_1
java setup_1
cd ..
cd ..
cd ..
cd ..

